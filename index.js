const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("public"));


app.get('/', function(req, res) {
res.render('login')
})

app.get('/token', function(req, res) {
  if (req.query.code) {

        let url = 'https://zoom.us/oauth/token?grant_type=authorization_code&code=' + req.query.code + '&redirect_uri=https://zoomapidemo.herokuapp.com/token';

        request.post(url, (error, response, body) => {

            // Parse response to JSON
            body = JSON.parse(body);

            // Logs your access and refresh tokens in the browser
            console.log(`access_token: ${body.access_token}`);
            console.log(`refresh_token: ${body.refresh_token}`);

            if (body.access_token) {

              res.render('token', {token: body.access_token})

            } else {
                    res.render('token', {token: "No Token"})
            }

        }).auth('YvTNwzdSRMGnKox4qmilkg', '347cz0P0TBJUUZmYRPxiy8Q6l1ath8Mp');

        return;

    }

    // Step 2:
    // If no authorization code is available, redirect to Zoom OAuth to authorize
    res.redirect('https://zoom.us/oauth/authorize?response_type=code&client_id=YvTNwzdSRMGnKox4qmilkg&redirect_uri=https://zoomapidemo.herokuapp.com/token')

})

app.post('/token', function(req, res) {
  res.status(301).redirect('https://zoom.us/logout');
});


app.listen(process.env.PORT || 3000, function(req, res) {
  console.log("Server is Running on 3000");
});
